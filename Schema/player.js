'use strict';
import mongoose from 'mongoose';
import {Schema} from 'mongoose';

const playerSchema = new Schema({
    name: String,
    createAt: {
        type: Date,
        default: Date.now()
    }
});

const Players = mongoose.model('Player', playerSchema);

export default Players;